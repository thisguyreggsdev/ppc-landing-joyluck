import React from 'react';
import Image from '../assets/3908595.jpeg';

const About = () => {
  const handleClickScroll = (sectionId) => () => {
    const section = document.getElementById(sectionId);
    if (section) {
      section.scrollIntoView({ behavior: 'smooth' });
    }
  };

  return (
    <div name="about" className="w-auto my-32 flex flex-col md:flex-row items-center mx-auto px-32">
      {/* Image */}
      <img src={Image} alt="" className="flex-none w-1/3 h-1/3 md:block hidden" />

      <div className="max-w-[1240px] mx-auto flex-1">
        <div className="text-auto">
          <h2 className="text-3xl font-bold ml-auto">Cleaning done right, every single time.</h2>
          <div className="text-2xl py-6 text-gray-500 text-justify mr-12">
            <p>
              At our cleaning services agency, we use only eco-friendly and non-toxic cleaning products to prioritize
              the health of your family, pets, and the planet. We offer flexible and customized cleaning services,
              whether it's a one-time deep cleaning or regular weekly cleanings. Our experienced team is dedicated to
              delivering thorough and efficient cleaning services, with a commitment to excellence and attention to
              detail. We guarantee your satisfaction with our services and are more than just a cleaning company - we're
              a partner in making your home a healthy and comfortable place to live.
            </p>
            <p>
              Our team takes pride in providing a personalized approach, tailoring our services to meet the specific
              requirements of each property owner. You can rely on our expert guidance and support to maximize the
              potential of your property while enjoying peace of mind. Sit back, relax and let us take care of
              everything.
            </p>
          </div>
        </div>
        <div className="grid md:grid-cols-3 gap-1 px-2 text-center mx-auto">
          <button className="py-3 sm:w-[70%] my-4" onClick={handleClickScroll('contact')}>
            Book now
          </button>
        </div>
      </div>
    </div>
  );
};

export default About;
