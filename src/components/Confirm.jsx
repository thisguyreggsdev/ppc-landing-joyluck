export default function Example() {
  setTimeout(() => {
    window.location.reload();
  }, 3000);

  return (
    <div className="rounded-md bg-green-50 p-4">
      <div className="flex">
        <div className="flex-shrink-0">
        </div>
        <div className="ml-3">
          <h3 className="text-xl font-medium text-gray-900">Your message is sent!</h3>
          <div className="mt-2 text-lg text-gray-900">
            <p>Our agents will be in touch with you soon.</p>
          </div>
          <div className="mt-4">
            <div className="-mx-2 -my-1.5 flex">
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
