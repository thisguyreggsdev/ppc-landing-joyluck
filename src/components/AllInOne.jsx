import React from 'react';

const tiers = [
  {
    name: 'Swift Clean',
    id: 'tier-basic',
    href: '#',
    price: { monthly: '$20', annually: '$600' },
    description: 'Everything necessary to get started.',
    features: ['1 cleaners', 'Twice a month', 'Up to 10% savings on conventional help', '48-hour support response time'],
  },
  {
    name: 'Deep Clean',
    id: 'tier-essential',
    href: '#',
    price: { monthly: '$40', annually: '$24' },
    description: 'Everything in Basic, plus essential tools for growing your business.',
    features: [
      '25 products',
      'Up to 10,000 subscribers',
      'Advanced analytics',
      '24-hour support response time',
      'Marketing automations',
    ],
  },
  {
    name: 'Overhaul',
    id: 'tier-growth',
    href: '#',
    price: { monthly: '$80', annually: '$48' },
    description: 'Everything in Essential, plus collaboration tools and deeper insights.',
    features: [
      'Unlimited products',
      'Unlimited subscribers',
      'Advanced analytics',
      '1-hour, dedicated support response time',
      'Marketing automations',
      'Custom reporting tools',
    ],
  },
]
 const handleClickScroll = (sectionId) => () => {
    const section = document.getElementById(sectionId);
    if (section) {
      section.scrollIntoView({ behavior: 'smooth' });
    }
  };

export default function Example() {
  return (
    <div className="bg-white py-24 sm:py-32">
      <div className="mx-auto max-w-7xl px-6 lg:px-8">
        <div className="mx-auto max-w-4xl text-center">
          <p className="mt-2 text-4xl font-bold tracking-tight text-gray-900 sm:text-5xl">
            Choose the right plan for&nbsp;you
          </p>
        </div>
        <p className="mx-auto mt-6 max-w-2xl text-lg leading-8 text-gray-600 text-center">
          We promise that our services meet your needs. Avail our different cleaning plans today!
        </p>
        <div className="mt-20">
          <div className="mt-20 grid gap-10 justify-center md:grid-cols-2 lg:grid-cols-3">
            {tiers.map((tier) => (
              <div key={tier.id} className="pt-16 px-8 lg:pt-0 flex flex-col items-center">
                <h3 id={tier.id} className="text-base font-semibold leading-7 text-gray-900 text-center">
                  {tier.name}
                </h3>
                <p className="mt-6 flex items-baseline justify-center gap-x-1">
                  <span className="text-5xl font-bold tracking-tight text-gray-900">{tier.price.monthly}</span>
                  <span className="text-sm font-semibold leading-6 text-gray-600">/hour</span>
                </p>
                <p className="mt-3 text-sm leading-6 text-gray-500 text-center">*All cleaning services included</p>
                <button className="mt-10 block rounded-md bg-indigo-600 px-3 py-2 text-center text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600" onClick={handleClickScroll('contact')}>
                  Book now
                </button>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}
