import React from 'react';
import bgImg from '../assets/bg5.jpg';

const Hero = () => {
  const handleClickScroll = (sectionId) => () => {
    const section = document.getElementById(sectionId);
    if (section) {
      section.scrollIntoView({ behavior: 'smooth' });
    }
  };

  return (
      <div 
      name='home' 
      className='w-auto h-screen flex flex-col justify-between'
      style={{ 
        backgroundImage: `url(${bgImg})`,
        backgroundSize: 'cover', 
        backgroundPosition: 'center', 
        backgroundRepeat: 'no-repeat'
      }} >
      <div className='grid md:grid-cols-2 max-w-[1240px] m-auto'>
        <div className='flex flex-col justify-center md:items-start w-full px-2 py-8'>
          <h1 className='py-3 text-5xl md:text-7xl font-bold'>Your trusted cleaning services.</h1>
          <button className='py-3 px-6 sm:w-[60%] my-4' onClick={handleClickScroll('contact')}>
            Book now
          </button>
        </div>
      </div>
    </div>
  );
};

export default Hero;
