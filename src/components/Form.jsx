import React, { useRef, useState } from 'react';
import { useForm, ValidationError } from '@formspree/react';
import ConfirmButton from './Confirm';
import Image2 from '../assets/image2.png'
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';


function ContactForm() {
  const [selectedDate, setSelectedDate] = useState(null);

  const [state, handleSubmit] = useForm('xrgvraar');
  const formRef = useRef(null);

  if (state.succeeded) {
    return <ConfirmButton />;
  }

  return (
    <form
      onSubmit={handleSubmit}
      className="w-full max-w-screen-lg lg:flex lg:items-center lg:justify-between"
      ref={formRef}
    >
      <div className="w-full lg:w-1/2 lg:pr-10 mb-6 lg:mb-0">
        <h1 className="text-6xl text-gray-900 font-bold text-center lg:text-left mb-8 pt-12">
          Book a service
        </h1>
        <div className="mb-4">
          <label htmlFor="name" className="block mb-2 font-bold">
            Full Name
          </label>
          <input
            id="name"
            type="text"
            name="fullname"
            className="w-full border py-2 px-3"
          />
          <ValidationError
            prefix="Full Name"
            field="Full Name"
            errors={state.errors}
          />
        </div>
        <div className="mb-4">
          <label htmlFor="email" className="block mb-2 font-bold">
            Email Address
          </label>
          <input
            id="email"
            type="email"
            name="email"
            className="w-full border py-2 px-3"
          />
          <ValidationError
            prefix="Email"
            field="email"
            errors={state.errors}
          />
        </div>
        <div className="mt-5">
                <label htmlFor="services" className="block mb-2 font-bold">
                  Services
                </label>
                <div className="mt-2.5">
                  <select
                    name="services"
                    id="services"
                    required
                    className="w-full border py-2 px-3"
                  >
                    <option value="others">Please select your needed services:</option>
                    <option value="detailing">Swift Clean</option>
                    <option value="clean&buff">Deep Clean</option>
                    <option value="inspection">Overhaul</option>
                    <option value="others">Others specify in messsage</option>
                  </select>
                </div>
              </div>
              <div>
                <div className="mt-5">
                  <label htmlFor="appointment" className="block mb-2 font-bold">
                    Pick a appointment
                  </label>
                  <div className="mt-2.5">
                    <DatePicker
                      id="appointment"
                      selected={selectedDate}
                      onChange={(date) => setSelectedDate(date)}
                      minDate={new Date()}
                      required
                      className="w-full border py-2 px-3"
                    />
                  </div>
                </div>
              </div>

        <div className="mb-8">
          <label htmlFor="message" className="block mb-2 font-bold">
            Message
          </label>
          <textarea
            id="message"
            name="message"
            rows="8"
            className="w-full border py-2 px-3 resize-none"
          />
          <ValidationError
            prefix="Message"
            field="message"
            errors={state.errors}
          />
        </div>
        <button
          type="submit"
          disabled={state.submitting}
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-10 rounded"
        >
          Submit
        </button>
      </div>
      <div className="hidden lg:block lg:w-3/4">
        <img src={Image2} alt="" className="w-full h-auto object-cover" />

      </div>
    </form>
  );
}

function App() {
  return (
    <div className="bg-white-800 flex justify-center items-center">
      <ContactForm />
    </div>
  );
}

export default App;
