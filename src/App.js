import About from './components/About';
import AllInOne from './components/AllInOne';
import Footer from './components/Footer';
import Hero from './components/Hero';
import Navbar from './components/Navbar'
import Form from './components/Form';
import Pricing from './components/Pricing';

function App() {
  return (
    <>
      <Navbar />
      <Hero />
      <div id="about">
      <About />
      </div>
      <AllInOne />
      <Pricing/>
      <div id="contact">
      <Form/>
      </div>
      <Footer />
    </>
  );
}

export default App;
